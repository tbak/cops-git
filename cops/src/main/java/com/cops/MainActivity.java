package com.cops;

import android.app.Activity;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBarActivity;


import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.io.IOException;
import java.io.InputStream;

class Person {
    public String mLastName;
    public String mFirstName;
    public double mLocationLatitude;
    public double mLocationLongitude;
    public int mHeight;
    public int mWeight;
    public String mDateOfBirth;
    public String mEyeColor;
    public String mHairColor;
    public String mDateOfParole;

    private Person() { }
    public Person( String lastName, String firstName, double locationLat, double locationLong, int height, int weight, String dateOfBirth, String eyeColor, String hairColor, String dateOfParole ) {
        mLastName = lastName;
        mFirstName = firstName;
        mLocationLatitude = locationLat;
        mLocationLongitude = locationLong;
        mHeight = height;
        mWeight = weight;
        mDateOfBirth = dateOfBirth;
        mEyeColor = eyeColor;
        mHairColor = hairColor;
        mDateOfParole = dateOfParole;
    }
}

class Crime {
    public int mID;
    public String mCharge;
    public double mLocationLatitude;
    public double mLocationLongitude;

    private Crime() {  }

    public Crime( int id, String charge, double locationLat, double locationLong ) {
        mID = id;
        mCharge = charge;
        mLocationLatitude = locationLat;
        mLocationLongitude = locationLong;
    }
}

class SimpleCSV {

    private String mFilename;
    public SimpleCSV( String filename ) {
        mFilename = filename;
    }

    protected String[] readFile( Activity activity ) {
        AssetManager am = activity.getAssets();
        InputStream inputStream = null;
        try {

            inputStream= am.open( mFilename );

            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            // byte buffer into a string
            String text = new String(buffer);

            // TB TODO - Gotta close the file?
            String[] lines = text.split("\n");

            return lines;

        } catch (IOException e) {
            return null;
        }

    }
}

class CrimesCSV extends SimpleCSV {
    final static String TAG = "CrimesCSV";
    public Crime[] mCrimes;

    public CrimesCSV( Activity activity ) {
        super("crimes.csv");
        String[] lines = readFile(activity);

        mCrimes = new Crime[lines.length-1];
        for( int i = 0; i < lines.length-1; i++ ) {

            // Skip the first line, since that's the header
            String line = lines[i+1];
            Log.v(TAG, "Line:" + line);
            String [] fields = line.split(",");

            int id = Integer.parseInt( fields[0] );
            String charge = fields[1];
            double latitude = Double.parseDouble( fields[2] );
            double longitude = Double.parseDouble( fields[3] );
            mCrimes[i] = new Crime( id, charge, latitude, longitude );
        }

    }

}

class PeopleCSV extends SimpleCSV {
    final static String TAG = "PeopleCSV";
    public Person[] mPeople;

    public PeopleCSV( Activity activity ) {
        super("people.csv");
        String[] lines = readFile(activity);

        mPeople = new Person[lines.length-1];
        for( int i = 0; i < lines.length-1; i++ ) {

            // Skip the first line, since that's the header
            String line = lines[i+1];
            Log.v(TAG, "Line:" + line);
            String [] fields = line.split(",");

            String lastName = fields[0];
            String firstName = fields[1];
            double latitude = Double.parseDouble(fields[2]);
            double longitude = Double.parseDouble(fields[3]);
            int height = Integer.parseInt(fields[4]);
            int weight = Integer.parseInt( fields[5] );
            String dateOfBirth = fields[6];
            String eyeColor = fields[7];
            String hairColor = fields[8];
            String dateOfParole = fields[9];

            mPeople[i] = new Person( lastName, firstName, latitude, longitude, weight, height, dateOfBirth, eyeColor, hairColor, dateOfParole );
        }

    }

}


public class MainActivity extends ActionBarActivity {

    final static String TAG = "MainActivity";
    private Switch mSwitchCrimes;
    private Switch mSwitchPeople;
    private GoogleMap mMap;


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter(){
            myContentsView = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.title));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }


    private void rebuildMapMarkers() {
        mMap.clear();

        if( mSwitchCrimes.isChecked() ) {
            // TB TODO - Reloading the CSV very time is a waste
            CrimesCSV crimesCSV = new CrimesCSV(this);
            for( int i = 0; i < crimesCSV.mCrimes.length; i++ ) {
                Crime crime = crimesCSV.mCrimes[i];

                final LatLng loc = new LatLng( crime.mLocationLatitude, crime.mLocationLongitude );
                mMap.addMarker(new MarkerOptions().position(loc).title("CRIME").snippet(crime.mCharge).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_crime)));
            }
        }

        if( mSwitchPeople.isChecked() ) {
            // TB TODO - Reloading the CSV very time is a waste
            PeopleCSV peopleCSV = new PeopleCSV(this);
            for( int i = 0; i < peopleCSV.mPeople.length; i++ ) {
                Person person = peopleCSV.mPeople[i];

                final LatLng loc = new LatLng( person.mLocationLatitude, person.mLocationLongitude );
                mMap.addMarker(new MarkerOptions().position(loc).title("PERSON").snippet(person.mFirstName + " " + person.mLastName));
            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwitchCrimes = (Switch) findViewById(R.id.switch_crimes);
        mSwitchPeople = (Switch) findViewById(R.id.switch_people);

        mSwitchCrimes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rebuildMapMarkers();
            }
        });
        mSwitchPeople.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rebuildMapMarkers();
            }
        });

        mMap = ((SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        //map.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);

        /*
        final LatLng SYDNEY = new LatLng(-33.88,151.21);
        final LatLng SYDNEY2 = new LatLng(-32.88,151.21);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, 15));

        map.addMarker(new MarkerOptions().position(SYDNEY).title("Marker").snippet("The most populous city in Australia."));


        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.abc_ic_search))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(SYDNEY2));

        map.addPolyline(new PolylineOptions().geodesic(true)
            .add(new LatLng(-33.866, 151.195))  // Sydney
            .add(new LatLng(-18.142, 178.431))  // Fiji
            .add(new LatLng(21.291, -157.821))  // Hawaii
            .add(new LatLng(37.423, -122.091)));  // Mountain View

        map.setInfoWindowAdapter(new MyInfoWindowAdapter());
        */

        final LatLng CRIME_CENTER = new LatLng(34.1470, -118.120);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CRIME_CENTER, 15));

        rebuildMapMarkers();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickCrimesSwitch( View v ) {
        Log.v(TAG, "Click crimes!");
    }
    public void onClickPeopleSwitch( View v ) {
        Log.v(TAG, "Click people!");
    }

}



